#!/bin/bash

cd "$(dirname "$0")"

[ -d ~/.logseq_bckup ] || mv ~/.logseq{,_bckup}
[ -d ~/.config/Logseq_bckup ] || mv ~/.config/Logseq{,_bckup}

ln -s "$(realpath .logseq)" ~/.logseq
ln -sT "$(realpath config_logseq/)" ~/.config/Logseq

# create Preferences file from template if it does not exist yet
[ -f config_logseq/Preferences ] || cp config_logseq/Preferences{.template,}
