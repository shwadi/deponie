# Jitsi quik (and dirty) launch

This script brings up an jitsi server in a quick and dirty way, which means that jitsi will be accessable
* via the public ip (if the required ports are exported; see below)
* with selfsigned certificates

Required Tools:
* git
* docker-compose

Required port-forwardings:
* TCP 8443
* TCP 8000
* UDP 10000
