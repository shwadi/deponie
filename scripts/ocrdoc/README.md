# ocrdoc

This is an old script of mine which I used to guide me through the process of creating pdfs with an ocr-generated text layer for scanned documents which required some image preprocessing to gain optimal results.

## requirement

* Access to a configured, connected and active scanner
* Imageprocessingtool (default: gimp)

## Usage

The script is launched with the desired path where the project folder shall be placed.

Further options can be determined by launching `orcdoc -h`.
