# russian-morphology

# rusmorph

Simple script to access the morphology of russian words from the commandline.

Included features:
* caching (previously queried words are accessible offline)

Included antifeatures:
* as a quick and dirty solution the downloaded content is parsed via the `vim` editor which is quite time consuming

Required tools:
* lynx

## usage

```bash
rusmorph <word_of_interest>
```

# rusmorph-learn

Simple interactive script for repeating/lerning the morphology of cached words (obtained by using `rusmorph`).

# rusmorph-gen-anki-csv

Simple script to translate the cached words (obtained by using `rusmorph`) to a csv file importable by [anki](https://ankiweb.net).
