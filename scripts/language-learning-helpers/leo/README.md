# leo

Simple script to access the leo.org dictionary from the commandline.

A more feature-rich tool with the same name can be found in the debian package `libwww-dict-leo-org-perl`.

Included features:
* caching (previously queried words are accessible offline)
* current dictionaries: `de <-> en` and `de <-> ru`

Required tools:
* lynx

## usage

```bash
leo <en|ru> <word_to_translate>
```
