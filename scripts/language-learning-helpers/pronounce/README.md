# pronounce

Simple script to play the pronounciation of provided words from the commandline.

Included features:
* caching (previously queried pronounciations are accessible offline)
* current languages: en (using [[merriam-webster.com]])

## usage

```bash
pronounce <word_to_pronounce>
```
