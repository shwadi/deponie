#!/bin/bash

main(){
    local document_to_be_signed=$1 signing_template=$2
    local print_help=false
    if [ "$1" = -h ]; then
	print_help=true
    elif [ $# -ne 2 ]; then
	print_help=true
    else
	local file
	while read file; do
	    if ! LANG=C file 2>/dev/null "$file" | grep -q PDF; then
		echo >&2 "$file is not a valid pdf-file"
		return
	    fi
	done <<< $(echo -e "$1\n$2")
    fi
    local result="${document_to_be_signed%%.pdf}_signed.pdf"
    $print_help && echo >&2 "usage: $0 <document_to_be_signed> <signing_template>" && return
    pdftk "$document_to_be_signed" multistamp $signing_template output "$result"
}

main "$@"
