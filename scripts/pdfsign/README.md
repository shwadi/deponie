# pdfsign

A simple script which takes the path to two pdf files and lays the latter one (transparent document with a signature) above the prior one (document to be signed) via `pdftk`.
