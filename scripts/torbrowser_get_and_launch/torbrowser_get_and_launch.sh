#!/bin/bash

errecho(){ echo >/dev/stderr "$*"; }
die(){ errecho "$*"; exit 1; }

DESTINATION="$HOME/Downloads/tor"
EXTRACTED_SUBFOLDER=newest_extracted
SIGNING_KEY="EF6E 286D DA85 EA2A 4BA7  DE68 4E2C 6E87 9329 8290"

main(){
    local current_releases
    if ! current_releases=$(2>/dev/null wget -O - https://dist.torproject.org/torbrowser/); then
        die "Failed to access 'https://dist.torproject.org/torbrowser/'"
    fi
    local newest_version=$(<<< "$current_releases" grep -Eo 'folder.*href="[^"]+' | sed 's/.*"//' | tr -d '/' | sort -V | tail -1)
    local archive="tor-browser-linux-x86_64-$newest_version.tar.xz"
    mkdir -p "$DESTINATION"
    cd "$DESTINATION"
    local extension
    for extension in '' .asc; do
        local file="$archive$extension"
        if [ -f "$file" ]; then
            errecho "Reusing cached '$file' file."
        else
            local url="https://www.torproject.org/dist/torbrowser/$newest_version/tor-browser-linux-x86_64-$newest_version.tar.xz$extension"
            errecho "Downloading '$url' ..."
            if ! wget "$url" -O "$file"; then
                die "Failed to download '$url'"
            fi
        fi
    done


    if ! gpg --list-keys "$(tr -d ' ' <<< "$SIGNING_KEY")"; then
        if ! gpg --recv-keys "0x$(tr -d ' ' <<< "$SIGNING_KEY")"; then
            die "Failed to receive missing signing key '$SIGNING_KEY'"
        fi
    fi

    local verification
    verification=$(2>&1 LANG=C gpg --verify "$archive"{.asc,}) || die "Verification failed"
    local regexp
    for regexp in '^gpg: Good signature' "^Primary key fingerprint: $SIGNING_KEY"; do
        grep -q "$regexp" <<< "$verification" || die "Verification did not match the regexp '$regexp'"
    done

    echo -e "\n$verification\n"

    rm -rf "$EXTRACTED_SUBFOLDER"
    mkdir -p "$EXTRACTED_SUBFOLDER"

    errecho "Extracting the archive of version $newest_version ..."
    tar -xf "$archive" -C "$EXTRACTED_SUBFOLDER"

    errecho "Launching torbrowser ..."
    "./$EXTRACTED_SUBFOLDER/tor-browser/Browser/start-tor-browser"
}

main
