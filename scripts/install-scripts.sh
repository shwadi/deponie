#!/bin/bash

cd "$(dirname "$(realpath "$0")")"

DEST_FOLDER=$HOME/.local/bin

mkdir -p "$DEST_FOLDER"
find . -executable -type f |
    # drop this script
    grep -v "$0" |
    # create symlinks in $DEST_FOLDER
    DEST_FOLDER=$DEST_FOLDER xargs -I% bash -c 'ln -fs "$(realpath %)" "$DEST_FOLDER"/$(basename %)'

bash -ic 'echo $PATH' | grep -qF "$DEST_FOLDER" || echo "PATH=$PATH:$DEST_FOLDER" >> ~/.bashrc
