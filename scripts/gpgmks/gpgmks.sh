#!/bin/sh

#2do:
# * complete cleanup

#=== ALIASES AND HELPER-FUNCTIONS ===
alias echo="printf '%b\n'"
alias removeInlineComments="sed -E 's/[[:space:]]*#.*//'"
alias removeCommentLines="sed -E '/^[[:space:]]*#/d'"
alias removeAllComments="removeCommentLines | removeInlineComments"
alias stripLeadingWhitespace="sed -E 's/^[[:space:]]*//'"
alias stripTrailingWhitespace="sed -E 's/[[:space:]]*$//'"
alias stripAllWhitespace="stripLeadingWhitespace | stripTrailingWhitespace"
alias indent="sed 's/^/    /'"
alias indent_all_but_first_line="sed -E '2,\$s/^/    /'"

printinfo(){ echo "Info: $*" | indent_all_but_first_line > /dev/stderr; }

printerror(){ echo "Error: $*" | indent_all_but_first_line > /dev/stderr; }

die(){ printerror "$*"; exit 1; }

determine_tmpfolder(){
    TMPFOLDER="$(head -n 1 "$TMPFOLDERNOTE")"
    if [ -z "$TMPFOLDER" -o ! -d "$TMPFOLDER" ]; then
	TMPFOLDER="$(mktemp -d)"
	printinfo "New temporary folder created: '$TMPFOLDER'"
    else
	printinfo "Reusing temporary folder: '$TMPFOLDER'"
    fi
    echo "$TMPFOLDER" > "$TMPFOLDERNOTE"
}

remove_whitespace_indent(){
    local _input _shortest_indent
    _input="$(cat -)"
    _shortest_indent="$(echo "$_input" | sed 's/[^ ].*//' | sort -r -z | head -n 1)"
    echo "$_input" | sed "s/^$_shortest_indent//"
}

#===== SETUP =====
SCRIPTNAME=$(basename $0)
cd "$(dirname $0)"
SCRIPT_DIR="$PWD"
CONFIG="$SCRIPT_DIR/config"
DSTFOLDER="$SCRIPT_DIR/result"
TMPFOLDERNOTE="$SCRIPT_DIR/currently_used_tempfolder"
determine_tmpfolder

# make sure gpg2 will allways operate in the temporary environment
export GNUPGHOME="$TMPFOLDER"
alias gpg2="LANG=C gpg2 --no-default-keyring \
--keyring $TMPFOLDER/tmp-public-kr.gpg \
--secret-keyring $TMPFOLDER/tmp-private-kr.gpg \
--trustdb-name $TMPFOLDER/tmp-trustdb.gpg"

#=== GLOBAL VARIABLES ===
TIMESTAMP=$(date "+%Y-%m-%d_%H:%M:%S" | tr -d '\n')
INTERACTIVE_MODE=false
VERBOSE=false
CLEANUP=true
MASTER_ID=''
REVOKE_OLD_SUBKEYS=false


gpg2_dialog_wrapper(){
    local _key_operated_on="$1" _instructions _batch_instruction="--batch" _output _failed=false
    local _msg="" _raw_instructions
    shift

    _raw_instructions="$(cat - | remove_whitespace_indent)"
    _instructions="$( echo "$_raw_instructions" | removeAllComments)"

    if $INTERACTIVE_MODE; then
        [ -n "$_instructions" ] && \
            _msg="\nThese instructions would be used in non-interactive-mode:\n$_raw_instructions"
        printinfo "Executing command 'gpg $@'$_msg"
        gpg2 --pinentry-mode loopback "$@" && _failed=false
    else
        # gpg2 refuses to accept the batch option for some actions.
        # In non-batch-mode gpg2 writes its output directly to
        # the tty, which is very unfortunate in non-verbose mode.
        # To avoid this, use --no-tty if --batch is unsupported.
        [ "$batch_mode_forbidden" = "true" ] && _batch_instruction="--no-tty"
        batch_mode_forbidden=false #set back to default
        $VERBOSE && [ "$_batch_instruction" = "--no-tty" ] && _batch_instruction=""

        _output="$(\
                echo "$_instructions" | gpg2 \
                    --pinentry-mode loopback --command-fd 0 $_batch_instruction \
                    --passphrase "$(get_passphrase_for $_key_operated_on)" "$@" 2>&1
                )" && _failed=false

        $VERBOSE && [ -n "$_output" ] && echo "$_output" | indent
    fi
    $_failed && die "Something went wrong!\n$_output"
}

gpg2_wrapper(){ echo -n "" | gpg2_dialog_wrapper "$@"; }

gen_master_key(){
    local _keygen_instructions

    [ -n "$NAME_COMMENT" ] && NAME_COMMENT="Name-Comment: $NAME_COMMENT"
    [ -n "$NAME_EMAIL" ] && NAME_EMAIL="Name-Email: $NAME_EMAIL"

    _keygen_instructions="\
        # These instructions are not applicable in interactive mode
        Key-Type: RSA # 4 in interactive mode
        Key-Usage: cert,sign
        Key-Length: $KEYLEN
        Expire-Date: 0 # Master key shall not expire
        Name-Real: $NAME_REAL
        $NAME_COMMENT
        $NAME_EMAIL
        %commit"

    printinfo "Creating new masterkey"
    echo "$_keygen_instructions" | gpg2_dialog_wrapper "new_key" \
	    --enable-large-rsa --full-generate-key
}

gen_master_revocation_cert(){
    local _master_id=$1 _purpose=$2 _instructions _output batch_mode_forbidden

    _output="$DSTFOLDER/${TIMESTAMP}_master_revocation_${_master_id}.asc"

    printinfo "Generating master revocation certificate"

    _instructions="\
        y # Create a revocation certificate for this key? (y/N)
        # Please select the reason for the revocation:
        0 # 0 = No reason specified
          # 1 = Key has been compromised
          # 2 = Key is superseded
          # 3 = Key is no longer used
        # Enter an optional description; end it with an empty line:

        y # Is this okay?
        "
    echo "$_instructions" | batch_mode_forbidden=true gpg2_dialog_wrapper "$_master_id" \
        --output "$_output" --gen-revoke "$_master_id"

    [ ! -e "$_output" ] && die "Failed to create the revocation-certificate for $_master_id"

    if [ "$_purpose" = "import" ]; then
        import_keyfile "$_output"
    elif [ "$_purpose" = "store" ]; then
        _instructions="\
            To avoid an accidental use of this file, a colon has been inserted
            before the 5 dashes below.  Remove this colon with a text editor
            before importing and publishing this revocation certificate.

            :"
        _instructions=$(echo "$_instructions" | stripLeadingWhitespace)
        _instructions="$_instructions$(cat "$_output")"
        echo "$_instructions" > ${_output}
    else
        die "$0 unknown purpose $_purpose"
    fi

    printinfo "Revocation certificate written to:\n$_output"
}

export_secret_master(){
    local _master_id="$1" _output_dst _msg

    _output_dst="$DSTFOLDER/${TIMESTAMP}_master_${_master_id}_secret.asc"

    gpg2_wrapper "$_master_id" --output "$_output_dst" --armor --export-secret-keys "$_master_id"

    is_keyfile_valid "$_output_dst" || die "Failed to export secret-key '$_master_id'"

    _msg="Keep this file secret; there is no need to import it into your real keyring!"
    printinfo "Secret key '$_master_id' exported to:\n$_output_dst\n$_msg"
}

export_secret_subkeys(){
    local _master_id=$1 _dsc=$2 _output_dst

    _output_dst="$DSTFOLDER/${TIMESTAMP}_master_${_master_id}_subkeys_${_dsc}.asc"

    gpg2_wrapper "$_master_id" --output "$_output_dst" --armor --export-secret-subkeys "$_master_id"

    is_keyfile_valid "$_output_dst" || die "Failed to export secret-subkeys of '$_master_id'"

    _msg="You need to import this keyfile into your real keyring."
    printinfo "Secret key '$_master_id' exported to:\n$_output_dst\n$_msg"
}

gen_new_subkeys(){
    local _master_id="$1" _edit_instructions _authkey_instructions
    if [ "$CREATE_AUTH_SUBKEY" = "true" ]; then
        _authkey_instructions="\
            addkey
            8 #(RSA set your own capabilities)
            # Next we will toggle [S]ign & [E]ncryption off
            # and [A]uthentication on before [Q]uitting
            S
            E
            A
            Q
            $KEYLEN
            $SUBKEY_EXPIRE"
    fi

    _edit_instructions="\
        addkey
        4 #rsa sign only
        $KEYLEN
        $SUBKEY_EXPIRE

        addkey
        6 #rsa encrypt only
        $KEYLEN
        $SUBKEY_EXPIRE

        $_authkey_instructions

        save"

    printinfo "Generating new subkeys"
    echo "$_edit_instructions" | stripLeadingWhitespace | \
        gpg2_dialog_wrapper "$_master_id" --expert --yes --edit-key "$_master_id"
}

revoke_all_subkeys(){
    local _master_id="$1" _unrevoked_subkeys _instructions _proof

    printinfo "revoking all subkeys of masterkey $_master_id"

    _unrevoked_subkeys=$(gpg2 --with-colons --list-key $_master_id 2>/dev/null | \
        awk -F: '/sub:[^r]/ {print $5}' | tr '\n' ' ')

    if [ -z "$_unrevoked_subkeys" ]; then
        _proof=$(gpg2 --list-key --list-options=show-unusable-subkeys "$_master_id" 2>&1)
        die "There are no unrevoked subkeys for $_master_id (did you import them?):\n$_proof"
    fi

    for _subkey in $_unrevoked_subkeys; do
        _instructions="\
            key $_subkey
            revkey
            y # Create a revocation certificate for this key? (y/N)
            # Please select the reason for the revocation:
            #   0 = No reason specified
            #   1 = Key has been compromised
            #   2 = Key is superseded
            #   3 = Key is no longer used
            0
            # Enter an optional description; end it with an empty line:

            y # Is this okay?
            save"
        printinfo "revoking subkey $_subkey"
        echo "$_instructions" | stripLeadingWhitespace | removeAllComments | \
            gpg2_dialog_wrapper "$_master_id" --edit-key "$_master_id"

    done
}

new_keys_in_keyring(){
    local _old_keys=$* _all_keys _key _new_keys=""

    _all_keys=$(gpg2 --with-colons --list-keys 2>/dev/null | \
                awk -F: '/pub:/ {print $5}' | tr '\n' ' ')

    [ -z "$_old_keys" ] && echo "$_all_keys" | stripAllWhitespace && return

    echo "$_all_keys" | tr ' ' '\n' | while read _key; do
        [[ "$_old_keys" != *$_key* ]] && _new_keys="${_new_keys}$_key "
    done

    echo "$_new_keys" | stripAllWhitespace
}

query_passphrase_for_new_key(){
    local _done=false _passphrase1 _passphrase2

    until $_done; do
	echo "Please provide a passphrase for the new key"
        stty -echo; read -r _passphrase1; stty echo

	if [ -z "$_passphrase1" ]; then
	    echo "You have not entered a passphrase - this is in general a bad idea!"
	    echo "Type [y]es if you are sure that you do not want to have any protection on your key."
	    read _passphrase2
        else
            echo "Please repeat the passphrase"
            stty -echo; read -r _passphrase2; stty echo
	fi
	[ -n "$_passphrase1" ] && [ "$_passphrase1" = "$_passphrase2" ] && _done=true
	[ -z "$_passphrase1" ] && [ "$_passphrase2" = "y" ] && _done=true
    done

    store_passphrase_for new_key "$_passphrase1"
}

task_init(){
    local _keyid_collection _master_id

    _keyid_collection=$(new_keys_in_keyring)

    $INTERACTIVE_MODE || query_passphrase_for_new_key

    gen_master_key

    _master_id="$(new_keys_in_keyring $_keyid_collection)"

    [ -z "$_master_id" ] && die "Can't determine new masterkeyid"

    printinfo "New masterkey ($_master_id) created"

    store_passphrase_for "$_master_id" "$(get_passphrase_for new_key)"

    export_secret_master "$_master_id"

    gen_master_revocation_cert "$_master_id" store

    gen_new_subkeys "$_master_id"

    export_secret_subkeys "$_master_id" created
}

task_revsub(){
    local _master_id="$1"

    revoke_all_subkeys $_master_id

    export_secret_subkeys "$_master_id" "revoked"
}

task_gensub(){
    local _master_id="$1" _performed_action

    if $REVOKE_OLD_SUBKEYS; then
        printinfo "instructed to revoke old subkeys"
        revoke_all_subkeys $_master_id
        _performed_action="revokedOldSubkeys_"
    fi

    gen_new_subkeys "$_master_id"

    export_secret_subkeys "$_master_id" "${_performed_action}addedNewSubkeys"
}

read_keyid_from_keyfile(){
    cat "$1" 2>/dev/null | \
        gpg2 --show-key --with-colons 2>/dev/null | head -n 1 | awk -F: '{print $5}'
}

read_keyid_from_keyring(){
    local _keyid
    gpg2 --list-key --with-colons -- "$1" 2>/dev/null | grep '^pub' | awk -F: '{print $5}'
}

export_public_key(){
    local _key_id=$1 _location=$2

    gpg2 --armor --output $_location --export $_key_id && \
        printinfo "public-key '$_key_id' written to '$_location'"
}

is_key1_signedby_key2(){
    gpg2 --list-sigs --with-colons -- "$1" | grep -q "^sig.*$2"
}

task_sign(){
    local _master_id="$1" _key2sing _signkeyid _output_store_location
    shift

    while [ $# -gt 0 ]; do
        _key2sign="$1" ; shift

        # assuming $_key2sign is a valid gpg-key-path
        _signkeyid=$(read_keyid_from_keyfile $_key2sign)

        if [ -n "$_signkeyid" ];then
            import_keyfile "$_key2sign"
            _output_store_location="${_key2sign}.signed"
        else
            # $_key2sign is not a valid gpg-key-path
            # check if it represents an imported key
            read_keyid_from_keyring "$_key2sign"
            _signkeyid=$(read_keyid_from_keyring "$_key2sign")
            if [ $(echo "$_signkeyid" | wc -l) -gt 1 ]; then
                printerror "tmp-keyring contains multiple keys matching '$_key2sign'"
                _signkeyid=""
            fi
            _output_store_location="./result/$_signkeyid.signed"
        fi

        if [ -z "$_signkeyid" ]; then
            printerror "can't sign '$_key2sign'"
        else
            if is_key1_signedby_key2 "$_signkeyid" "$_master_id"; then
                printinfo "'$_signkeyid' is already signed by '$_master_id'"
            else
                printinfo "signing $_signkeyid ..."
                gpg2 --sign-key --ask-cert-level --default-key $_master_id -- "$_signkeyid" && \
                    export_public_key "$_signkeyid" "$_output_store_location"
            fi
        fi

    done
}

task_revmaster(){
    local _master_id=$1 _msg _decision

    printinfo "Inter: $INTERACTIVE_MODE"
    if ! $INTERACTIVE_MODE; then
        _msg="\
            This task is running in non-interactive mode.
            It would be reasonable to switch to this mode,
            if you wich to create a customized certificate.
            Do you wish to create a customized certificate? (y/n)"
        echo "$_msg" | stripLeadingWhitespace
        read -r _decision
        if [ "$_decision" = "y" ]; then
            printinfo "Switching to interactive mode"
            INTERACTIVE_MODE=true
        else
            printinfo "Not switching to interactive mode"
        fi
    fi

    gen_master_revocation_cert "$_master_id" "import"
}

task_noop(){ :; }

usage(){
    local _usage="\
        Syntax: $SCRIPTNAME [global options] [keyfiles] task [task-related arguments]
            'keyfiles': keyfiles, which shall be imported/merged into temporary keyring

        Global options:
            -i           : interactive mode: Guides you through your chosen task but
                           lets you directly communicate to gpg2. Usefull if you want
                           to get a better understanding of what is going on or if you
                           want do some manual changes in the process of the task.
            -m MASTER_ID : specify the master-id if you decide to import more then one
                           masterkey for whatever reason
            -n           : no cleanup (the temporary setup will not be destroyed before termination)
                           usefull in combination with noop-task, in case you want to source this
                           script into your shell to perform some more sophisticated actions
            -v           : verbose mode

        Tasks:
            init               : create new masterkey (with subkeys and revocation certificate)
            revsub             : revoke all subkeys
            gensub [-r]        : create new subkeys (revoke all previous in combination with -r)
            sign key2sign[...] : sign foreign keys (requires at least one path to keyfile)
            revmaster          : create yet another revocation certificate for the masterkey
            noop               : terminate after setup-initialization (see -n option)
        "
    echo "$_usage" | sed -E 's/^ {8}//' > /dev/stderr
    exit 1
}

read_keyfile_meta(){
    gpg2 --show-key --list-options=show-unusable-subkeys -- $1 2>/dev/null | sed 's/^/    /'
}

store_passphrase_for(){
    local _keyid="$1" _passphrase="$2" _global_var
    _keyid="$(echo $_keyid | stripAllWhitespace)"
    _global_var='PASSPHRASE_SAVE_'$_keyid

    eval "$_global_var='$_passphrase'"
}

get_passphrase_for(){
    local _keyid=$1 _global_var_access
    _keyid="$(echo $_keyid | stripAllWhitespace)"
    _global_var_access='${PASSPHRASE_SAVE_'$_keyid'}'

    eval "printf '%s\n' \"$_global_var_access\""
}

import_keyfile(){
    local _keyfile=$1 _keyid _loops  _key_imported _known_key_passphrase _passphrase _output _errmsg

    if grep -q "^:-----BEGIN PGP PUBLIC KEY BLOCK-----" "$_keyfile"; then
        printinfo "Skipping import of currently disabled file $_keyfile"
        printinfo "If you expected something different, edit the file and remove the colon."
        return
    fi

    printinfo "importing $_keyfile\n$(read_keyfile_meta $_keyfile)"
    _key_imported=false
    _keyid=$(read_keyid_from_keyfile $_keyfile)
    _known_key_passphrase="$(get_passphrase_for $_keyid)"
    _loops=0

    until $_key_imported; do
	case "$_loops" in
	    0)  # for the first time, try an empty or potentially known passphrase
		_passphrase="$_known_key_passphrase" ;;
	    1)  # prepare _errmsg in case that the _known_key_passphrase failed
		_errmsg="\
		    The known passphrase for $_keyid have been used.
		    If you have mixed up passphrases for the very same keyid in different files,
		    note that this is not supported. !!!consider to use manual option!!!"
		_errmsg=$(echo "$_errmsg" | stripLeadingWhitespace)
		_errmsg="Failed to import keyfile '$_keyfile':\n$_output\n\n$_errmsg"
		[ -n "$_known_key_passphrase" ] && die "$_errmsg"

		# otherwise continue
		echo "Please provide the passphrase for $_keyid" ;;
	    *)
		printerror "$_output"
		echo "You entered the wrong passphrase for $_keyid; please try again!"
		;;
	esac

	if [ "$_loops" -ne 0 ]; then
	    # read the passphrase silently (no echo)
	    stty -echo; read -r _passphrase; stty echo
	fi

	_output="$(gpg2 --pinentry-mode loopback \
	     --passphrase "$_passphrase" --import $_keyfile 2>&1)" && _key_imported=true

	_loops=$(($_loops + 1))
    done
    $VERBOSE && echo "$_output" | indent
    [ -z "$_known_key_passphrase" ] && store_passphrase_for "$_keyid" "$_passphrase"
}

is_keyfile_valid(){
    [ -e "$1" ] || return 1
    cat "$1" 2>/dev/null | gpg2 --show-key >/dev/null 2>&1 && return 0
    # We also accept gpg-files disabled by a colon
    grep -q "^:-----BEGIN PGP PUBLIC KEY BLOCK-----" "$1"
}

interprete_option(){
    local _scope=$1 _opt=$2 _arg=$3 _task_refuse_params=true _consumed=0

    if [ $# -gt 1 ]; then
        if [ "$_scope" = "global" ]; then
            _consumed=1
            case $_opt in
                -i) INTERACTIVE_MODE=true;;
                -n) CLEANUP=false;;
                -m) MASTER_ID=$_arg; _consumed=2;;
                -v) VERBOSE=true;;
                 *) _consumed=0;;
            esac
        else
            [ "$_scope" = "gensub" -a "$_opt" = "-r" ] && _consumed=1 && REVOKE_OLD_SUBKEYS=true

            [ "$_scope" = "sign" ] && _task_refuse_params=false
            $_task_refuse_params && [ "$_consumed" = "0" ] \
                die "unexpected option $_opt in task $_scope"
        fi
    fi

    interprete_option_consumed=$_consumed
}

check_master_id(){
    local _master_selector='*' _master_id _master_secret_indicator

    [ -n "$MASTER_ID" ] && _master_selector=$MASTER_ID

    _master_id=$(gpg2 --with-colons --list-secret-keys -- "$_master_selector" 2>/dev/null \
        | awk -F: '/sec:/ {print $5}')
    [ -z "$_master_id" ] && die "tmp-keyring contains no private key (thus no musterkey)"
    [ $(echo "$_master_id" | wc -l ) -gt 1 ] && \
        die "tmp-keyring contains multiple masterkey-candidates; consider using -m ..."

    _master_secret_indicator=$(gpg2 --list-secret-keys --with-colons | \
        awk -F: "/^sec:.*$_master_id/ {print \$15}")

    [ "$_master_secret_indicator" = "+" ] || \
        die "missing secret for masterkey '$_master_id'; check your imports"

    echo $_master_id
}

edit_and_source_config(){
    local _default_conf _editor

    is_config_missing_or_empty(){ [ ! -e "$CONFIG" ] || [ -z "$(cat "$CONFIG")" ]; }

    if is_config_missing_or_empty; then
        _default_conf="\
            # This file will be sourced by '$SCRIPTNAME'"'
            # so treat this file as a shell-script!
            # If you mess things up, delete all the content or the whole file
            # in order to regenerate the default-config by the next time.
            NAME_REAL="Max Mustermann"
            NAME_COMMENT=""
            NAME_EMAIL="max@muster.mann"
            KEYLEN=1024
            SUBKEY_EXPIRE=1y
            CREATE_AUTH_SUBKEY=true'
        echo "$_default_conf" | stripLeadingWhitespace > "$CONFIG"

        is_config_missing_or_empty && die "Failed to create defaultconfig"

        _editor="$EDITOR"
        until [ -e "$(which "$_editor")" ]; do
            [ -n "$_editor" ] && printerror "Editor '$_editor' is not installed"
            echo "Please provide your favourite texteditor to edit the config-file."
            read _editor
        done

        $_editor $CONFIG

        is_config_missing_or_empty && die "Missing config"
    fi

    printinfo "Sourcing configuration $CONFIG"
    . "$CONFIG"
}

cleanup(){
    : make sure the cleanup operates only on the desired files
    # local _purgecmd="rm -f "
    # which shred >/dev/null 2>&1 && _purgecmd="shred -vzun1 "
    # #find "$TMPFOLDER" -type f | sed 's/./\\&/g' | xargs echo $_purgecmd
    # find "$TMPFOLDER" -type f -exec echo $_purgecmd '{}' +
    # rm -rf $TMPFOLDER/*
    # mkdir -p $TMPFOLDER $DSTFOLDER
    # chmod 700 $TMPFOLDER $DSTFOLDER
}

main(){
    local _task interprete_option_consumed _master_id

    cleanup

    [ $# -eq 0 ] && usage

    until [ "$interprete_option_consumed" = "0" ]; do
        interprete_option "global" "$@"
        shift "$interprete_option_consumed"
    done

    while is_keyfile_valid "$1"; do
        import_keyfile "$1"
        shift
    done

    [ -z "$1" ] && die "no instructions given"

    for i in init revsub gensub sign revmaster noop; do
        [ "$1" = "$i" ] && _task="$1" && shift
    done

    [ -z "$_task" ] && die "invalid argument '$1'"

    if ! [ "$_task" = "init" -o "$_task" = "noop" ];then
        _master_id=$(check_master_id)
        [ -z "$_master_id" ] && die "Can't proceed without master-id"
        printinfo "assuming $_master_id to be the masterkey"
    fi

    interprete_option_consumed=""
    until [ "$interprete_option_consumed" = "0" ]; do
        interprete_option "$_task" "$@"
        shift $interprete_option_consumed
    done

    if [ "$_task" = "init" -o "$_task" = "gensub" ];then
        edit_and_source_config
    fi

    printinfo "Running task $_task"
    task_$_task "$_master_id" "$@"

    printinfo "Task $_task finished"

    # Explicitly unset all sensible variables
    for var in $(set | grep '^PASSPHRASE_SAVE_' | sed -E 's/=.*//'); do unset $var; done
}

main "$@"
